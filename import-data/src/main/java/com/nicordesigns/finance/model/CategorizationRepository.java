package com.nicordesigns.finance.model;

import org.springframework.data.repository.CrudRepository;

public interface CategorizationRepository extends CrudRepository<Categorization, String> {

}
