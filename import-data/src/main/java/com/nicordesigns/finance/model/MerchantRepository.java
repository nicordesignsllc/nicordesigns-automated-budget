package com.nicordesigns.finance.model;

import org.springframework.data.repository.Repository;

public interface MerchantRepository extends Repository<Merchant, String> {
}
